package List;

import java.util.ArrayList;

public class List3 {
    //общее количество
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(21);
        list.add(22);
        list.add(21);
        list.add(32);
        list.add(44);
        list.add(44);
        list.add(55);

        int all = 0;

        for (int i = 0; i < list.size() - 1; i++) {
            if (!list.get(i).equals(list.get(i + 1)))
                all++;
        }

        System.out.println(all);
    }
}
